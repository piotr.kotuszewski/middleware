package Model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2020-12-01T12:55:21.204+0100")
@StaticMetamodel(Customer.class)
public class Customer_ {
	public static volatile SingularAttribute<Customer, Long> id;
	public static volatile SingularAttribute<Customer, String> fullName;
	public static volatile SingularAttribute<Customer, String> customerNumber;
	public static volatile SingularAttribute<Customer, String> shortName;
	public static volatile SingularAttribute<Customer, String> customerType;
	public static volatile SingularAttribute<Customer, String> blocked;
	public static volatile SingularAttribute<Customer, String> closed;
	public static volatile SingularAttribute<Customer, String> deceased;
	public static volatile SingularAttribute<Customer, String> inactive;
	public static volatile SingularAttribute<Customer, String> accountOfficer;
	public static volatile SingularAttribute<Customer, String> reference;
	public static volatile SingularAttribute<Customer, String> language;
	public static volatile SingularAttribute<Customer, String> parentCountry;
	public static volatile SingularAttribute<Customer, String> riskCountry;
	public static volatile SingularAttribute<Customer, String> residenceCountry;
	public static volatile SingularAttribute<Customer, String> defaultBranch;
	public static volatile SingularAttribute<Customer, String> mailToBranch;
	public static volatile SingularAttribute<Customer, String> analysisCode;
	public static volatile SingularAttribute<Customer, String> bankCode1;
	public static volatile SingularAttribute<Customer, String> bankCode2;
	public static volatile SingularAttribute<Customer, String> bankCode3;
	public static volatile SingularAttribute<Customer, String> bankCode4;
	public static volatile SingularAttribute<Customer, String> customerGroup;
	public static volatile SingularAttribute<Customer, String> groupDescription;
	public static volatile SingularAttribute<Customer, String> dataMaintained;
	public static volatile SingularAttribute<Customer, String> midasFacilityAllow;
	public static volatile SingularAttribute<Customer, Integer> clearingId;
	public static volatile SingularAttribute<Customer, String> location;
}
