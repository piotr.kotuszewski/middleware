package Dao;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

import Model.Customer;

@RequestScoped
public class CustomerDaoImpl implements CustomerDao{
	
	@PersistenceUnit
	private EntityManagerFactory emFactory;

	@Override
	public void save(Customer customer) {
		EntityManager entityManager = emFactory.createEntityManager();
		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();
		entityManager.persist(customer);
		tx.commit();
		entityManager.close();
	}

	@Override
	public Customer get(Long id) {
		System.out.println("PRzed polaczeniem ------");
		EntityManager entityManager = emFactory.createEntityManager();
		System.out.println("COnnected -------");
		Customer customer = entityManager.find(Customer.class, id);
		System.out.println(customer.toString()+ " ---------------------CUSTOMER");
		entityManager.close();
		return customer;
	}


}
