package Dao;

import Model.Customer;

public interface CustomerDao {
	public void save(Customer customer);
	public Customer get(Long id);
}
