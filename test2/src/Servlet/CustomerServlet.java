package Servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Dao.CustomerDao;
import Model.Customer;

@WebServlet("/get")
public class CustomerServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private CustomerDao customerDao;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException{
		String customerId = request.getParameter("id");
		Long id = Long.valueOf(customerId);
		System.out.println(id+" ---------------------");
		Customer customer = customerDao.get(id);
		response.getWriter().print(customer);
		System.out.println(customer.toString());
	}
}
