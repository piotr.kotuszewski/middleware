package Database;

import java.sql.*;


public class Connection {
	public static void main(String [] args) {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "piotr", "admin123");
			Statement st = ((java.sql.Connection) con).createStatement();
			String sql = "SELECT * FROM customer";
			ResultSet rs = st.executeQuery(sql);
			while(rs.next())
				System.out.println(rs.getString(5));
			con.close();
			((java.sql.Connection) con).close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
