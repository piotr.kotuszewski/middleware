package Main;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

public class EntityManagerHelper {
    @PersistenceUnit
    EntityManagerFactory factory;
    
    @Produces
    @RequestScoped
    public EntityManager createEntityManager() {
        return factory.createEntityManager();
    }
    
    public void closeEntityManager(@Disposes EntityManager em) {
        em.close();
    }
}