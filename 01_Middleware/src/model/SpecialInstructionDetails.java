package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="special_instruction_detail")
public class SpecialInstructionDetails implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_special_instruction_detail")
	private long id;
	@Column(nullable=false)
	private String severity;
	private int code;
	private String details;
	@Column(name="style_code")
	private String styleCode;
	@Column(nullable=false)
	private String emphasis;
	private String type;
	@Column(name="business_area")
	private String businessArea;
	
	@ManyToOne
	@JoinColumn(name="id_customer")
	private Customer customer;
	
	public SpecialInstructionDetails() {}

	public SpecialInstructionDetails(String severity, int code, String details, String styleCode, String emphasis,
			String type, String businessArea) {
		super();
		this.severity = severity;
		this.code = code;
		this.details = details;
		this.styleCode = styleCode;
		this.emphasis = emphasis;
		this.type = type;
		this.businessArea = businessArea;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getStyleCode() {
		return styleCode;
	}

	public void setStyleCode(String styleCode) {
		this.styleCode = styleCode;
	}

	public String getEmphasis() {
		return emphasis;
	}

	public void setEmphasis(String emphasis) {
		this.emphasis = emphasis;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBusinessArea() {
		return businessArea;
	}

	public void setBusinessArea(String businessArea) {
		this.businessArea = businessArea;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "SpecialInstructionDetails [id=" + id + ", severity=" + severity + ", code=" + code + ", details="
				+ details + ", styleCode=" + styleCode + ", emphasis=" + emphasis + ", type=" + type + ", businessArea="
				+ businessArea + "]";
	}
	
	
}
