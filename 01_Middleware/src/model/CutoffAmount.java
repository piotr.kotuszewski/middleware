package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="cutoff_amount")
public class CutoffAmount implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private long id;
	@Column(nullable = false)
	private double ammount;
	@Column(nullable = false)
	private String currency;
	
	@ManyToOne
	@JoinColumn(name="id_other_details")
	private OtherDetails otherDetails;

	public CutoffAmount() {}

	public CutoffAmount(double ammount, String currency) {
		super();
		this.ammount = ammount;
		this.currency = currency;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getAmmount() {
		return ammount;
	}

	public void setAmmount(double ammount) {
		this.ammount = ammount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		return "CutoffAmount [id=" + id + ", ammount=" + ammount + ", currency=" + currency + "]";
	}
	
	
}
