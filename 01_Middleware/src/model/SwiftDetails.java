package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="swift_details")
public class SwiftDetails implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_swift_details")
	private long id;
	@Column(name="main_banking_entity")
	private String mainBankingEntity;
	@Column(name="swift_address")
	private String swiftAddress;
	@Column(nullable=false)
	private String authenticated;
	@Column(nullable=false)
	private String blocked;
	@Column(nullable=false)
	private String closed;
	@Column(name="transliteration_required")
	private String transliterationRequired;
	
	@ManyToOne
	@JoinColumn(name="id_customer")
	private Customer customer;
	
	public SwiftDetails() {}

	public SwiftDetails(String mainBankingEntity, String swiftAddress, String authenticated, String blocked,
			String closed, String transliterationRequired) {
		super();
		this.mainBankingEntity = mainBankingEntity;
		this.swiftAddress = swiftAddress;
		this.authenticated = authenticated;
		this.blocked = blocked;
		this.closed = closed;
		this.transliterationRequired = transliterationRequired;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMainBankingEntity() {
		return mainBankingEntity;
	}

	public void setMainBankingEntity(String mainBankingEntity) {
		this.mainBankingEntity = mainBankingEntity;
	}

	public String getSwiftAddress() {
		return swiftAddress;
	}

	public void setSwiftAddress(String swiftAddress) {
		this.swiftAddress = swiftAddress;
	}

	public String getAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(String authenticated) {
		this.authenticated = authenticated;
	}

	public String getBlocked() {
		return blocked;
	}

	public void setBlocked(String blocked) {
		this.blocked = blocked;
	}

	public String getClosed() {
		return closed;
	}

	public void setClosed(String closed) {
		this.closed = closed;
	}

	public String getTransliterationRequired() {
		return transliterationRequired;
	}

	public void setTransliterationRequired(String transliterationRequired) {
		this.transliterationRequired = transliterationRequired;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "SwiftDetails [id=" + id + ", mainBankingEntity=" + mainBankingEntity + ", swiftAddress=" + swiftAddress
				+ ", authenticated=" + authenticated + ", blocked=" + blocked + ", closed=" + closed
				+ ", transliterationRequired=" + transliterationRequired + "]";
	}
	
	
}
