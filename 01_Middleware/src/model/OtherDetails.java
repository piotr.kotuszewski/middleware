package model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="other_details")
public class OtherDetails implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_other_details")
	private long id;
	@Column(nullable=false)
	private String allow10TC;
	@Column(name="swift_ack_required", nullable=false)
	private String swiftAckRequired;
	@Column(name="transliterate_swift", nullable=false)
	private String transliterateSwift;
	@Column(name="corporate_access", nullable = false)
	private String corporateAccess;
	@Column(name="principal_fx_rate_code")
	private String principalFxRateCode;
	@Column(name="charge_fx_rate_code")
	private String chargeFxRateCode;
	@Column(name="charge_group")
	private String chargeGroup;
	@Column(name="allow_tax_exemptions", nullable=false)
	private String allowTaxExemptions;
	@Column(nullable=false)
	private String suspended;
	@Column(name="cutoff_fx_rate_code")
	private String cutoffFxRateCode;
	@Column(name="allow_interest_consolidation", nullable=false)
	private String allowInterestConsolidation;
	@Column(name="document_preparation_service")
	private String documentPreparationService;
	
	@ManyToOne
	@JoinColumn(name="id_customer")
	//@JsonIgnore
	private Customer customer;
	
	@OneToMany(mappedBy="otherDetails")
	private Set<CutoffAmount> cutoffAmounts;
	
	
	public OtherDetails() {}


	public OtherDetails(String allow10tc, String swiftAckRequired, String transliterateSwift, String corporateAccess,
			String principalFxRateCode, String chargeFxRateCode, String chargeGroup, String allowTaxExemptions,
			String suspended, String cutoffFxRateCode, String allowInterestConsolidation,
			String documentPreparationService) {
		super();
		allow10TC = allow10tc;
		this.swiftAckRequired = swiftAckRequired;
		this.transliterateSwift = transliterateSwift;
		this.corporateAccess = corporateAccess;
		this.principalFxRateCode = principalFxRateCode;
		this.chargeFxRateCode = chargeFxRateCode;
		this.chargeGroup = chargeGroup;
		this.allowTaxExemptions = allowTaxExemptions;
		this.suspended = suspended;
		this.cutoffFxRateCode = cutoffFxRateCode;
		this.allowInterestConsolidation = allowInterestConsolidation;
		this.documentPreparationService = documentPreparationService;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getAllow10TC() {
		return allow10TC;
	}


	public void setAllow10TC(String allow10tc) {
		allow10TC = allow10tc;
	}


	public String getSwiftAckRequired() {
		return swiftAckRequired;
	}


	public void setSwiftAckRequired(String swiftAckRequired) {
		this.swiftAckRequired = swiftAckRequired;
	}


	public String getTransliterateSwift() {
		return transliterateSwift;
	}


	public void setTransliterateSwift(String transliterateSwift) {
		this.transliterateSwift = transliterateSwift;
	}


	public String getCorporateAccess() {
		return corporateAccess;
	}


	public void setCorporateAccess(String corporateAccess) {
		this.corporateAccess = corporateAccess;
	}


	public String getPrincipalFxRateCode() {
		return principalFxRateCode;
	}


	public void setPrincipalFxRateCode(String principalFxRateCode) {
		this.principalFxRateCode = principalFxRateCode;
	}


	public String getChargeFxRateCode() {
		return chargeFxRateCode;
	}


	public void setChargeFxRateCode(String chargeFxRateCode) {
		this.chargeFxRateCode = chargeFxRateCode;
	}


	public String getChargeGroup() {
		return chargeGroup;
	}


	public void setChargeGroup(String chargeGroup) {
		this.chargeGroup = chargeGroup;
	}


	public String getAllowTaxExemptions() {
		return allowTaxExemptions;
	}


	public void setAllowTaxExemptions(String allowTaxExemptions) {
		this.allowTaxExemptions = allowTaxExemptions;
	}


	public String getSuspended() {
		return suspended;
	}


	public void setSuspended(String suspended) {
		this.suspended = suspended;
	}


	public String getCutoffFxRateCode() {
		return cutoffFxRateCode;
	}


	public void setCutoffFxRateCode(String cutoffFxRateCode) {
		this.cutoffFxRateCode = cutoffFxRateCode;
	}


	public String getAllowInterestConsolidation() {
		return allowInterestConsolidation;
	}


	public void setAllowInterestConsolidation(String allowInterestConsolidation) {
		this.allowInterestConsolidation = allowInterestConsolidation;
	}


	public String getDocumentPreparationService() {
		return documentPreparationService;
	}


	public void setDocumentPreparationService(String documentPreparationService) {
		this.documentPreparationService = documentPreparationService;
	}


	public Customer getCustomer() {
		return customer;
	}


	public void setCustomer(Customer customer) {
		this.customer = customer;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	


	public Set<CutoffAmount> getCutoffAmounts() {
		return cutoffAmounts;
	}


	public void setCutoffAmounts(Set<CutoffAmount> cutoffAmounts) {
		this.cutoffAmounts = cutoffAmounts;
	}


	@Override
	public String toString() {
		return "OtherDetails [id=" + id + ", allow10TC=" + allow10TC + ", swiftAckRequired=" + swiftAckRequired
				+ ", transliterateSwift=" + transliterateSwift + ", corporateAccess=" + corporateAccess
				+ ", principalFxRateCode=" + principalFxRateCode + ", chargeFxRateCode=" + chargeFxRateCode
				+ ", chargeGroup=" + chargeGroup + ", allowTaxExemptions=" + allowTaxExemptions + ", suspended="
				+ suspended + ", cutoffFxRateCode=" + cutoffFxRateCode + ", allowInterestConsolidation="
				+ allowInterestConsolidation + ", documentPreparationService=" + documentPreparationService
				+ ", cutoffAmounts=" + cutoffAmounts + "]";
	}
	
	
}
