package model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="customer")
public class Customer implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_customer")
	private long id;
	@Column(name="full_name")
	private String fullName;
	@Column(name="customer_number")
	private String customerNumber;
	@Column(name="short_name")
	private String shortName;
	@Column(name="customer_type")
	private String customerType;
	private String blocked;
	private String closed;
	private String deceased;
	private String inactive;
	@Column(name="account_officer")
	private String accountOfficer;
	private String reference;
	private String language;
	@Column(name="parent_country")
	private String parentCountry;
	@Column(name="risk_country")
	private String riskCountry;
	@Column(name="residence_country")
	private String residenceCountry;
	@Column(name="default_branch")
	private String defaultBranch;
	@Column(name="mail_to_branch")
	private String mailToBranch;
	@Column(name="analysis_code")
	private String analysisCode;
	@Column(name="bank_code_1")
	private String bankCode1;
	@Column(name="bank_code_2")
	private String bankCode2;
	@Column(name="bank_code_3")
	private String bankCode3;
	@Column(name="bank_code_4")
	private String bankCode4;
	@Column(name="customer_group")
	private String customerGroup;
	@Column(name="group_description")
	private String groupDescription;
	@Column(name="data_maintained")
	private String dataMaintained;
	@Column(name="midas_facility_allow")
	private String midasFacilityAllow;
	@Column(name="clearing_id")
	private int clearingId;
	private String location;
	
	@OneToMany(mappedBy="customer", fetch= FetchType.EAGER)
	private Set<AddressDetails> addressDetails;
	@OneToMany(mappedBy="customer", fetch= FetchType.EAGER)
	private Set<OtherDetails> otherDetails;
	@OneToMany(mappedBy="customer", fetch= FetchType.EAGER)
	private Set<SpecialInstructionDetails> specialInstructionDetails;
	
	public Customer() {}
	
	
	public Customer(Long id, String fullName, String customerNumber, String shortName, String customerType,
			String blocked, String closed, String deceased, String inactive, String accountOfficer, String reference,
			String language, String parentCountry, String riskCountry, String residenceCountry, String defaultBranch,
			String mailToBranch, String analysisCode, String bankCode1, String bankCode2, String bankCode3,
			String bankCode4, String customerGroup, String groupDescription, String dataMaintained,
			String midasFacilityAllow, int clearingId, String location) {
		
		this.id = id;
		this.fullName = fullName;
		this.customerNumber = customerNumber;
		this.shortName = shortName;
		this.customerType = customerType;
		this.blocked = blocked;
		this.closed = closed;
		this.deceased = deceased;
		this.inactive = inactive;
		this.accountOfficer = accountOfficer;
		this.reference = reference;
		this.language = language;
		this.parentCountry = parentCountry;
		this.riskCountry = riskCountry;
		this.residenceCountry = residenceCountry;
		this.defaultBranch = defaultBranch;
		this.mailToBranch = mailToBranch;
		this.analysisCode = analysisCode;
		this.bankCode1 = bankCode1;
		this.bankCode2 = bankCode2;
		this.bankCode3 = bankCode3;
		this.bankCode4 = bankCode4;
		this.customerGroup = customerGroup;
		this.groupDescription = groupDescription;
		this.dataMaintained = dataMaintained;
		this.midasFacilityAllow = midasFacilityAllow;
		this.clearingId = clearingId;
		this.location = location;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getBlocked() {
		return blocked;
	}
	public void setBlocked(String blocked) {
		this.blocked = blocked;
	}
	public String getClosed() {
		return closed;
	}
	public void setClosed(String closed) {
		this.closed = closed;
	}
	public String getDeceased() {
		return deceased;
	}
	public void setDeceased(String deceased) {
		this.deceased = deceased;
	}
	public String getInactive() {
		return inactive;
	}
	public void setInactive(String inactive) {
		this.inactive = inactive;
	}
	public String getAccountOfficer() {
		return accountOfficer;
	}
	public void setAccountOfficer(String accountOfficer) {
		this.accountOfficer = accountOfficer;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getParentCountry() {
		return parentCountry;
	}
	public void setParentCountry(String parentCountry) {
		this.parentCountry = parentCountry;
	}
	public String getRiskCountry() {
		return riskCountry;
	}
	public void setRiskCountry(String riskCountry) {
		this.riskCountry = riskCountry;
	}
	public String getResidenceCountry() {
		return residenceCountry;
	}
	public void setResidenceCountry(String residenceCountry) {
		this.residenceCountry = residenceCountry;
	}
	public String getDefaultBranch() {
		return defaultBranch;
	}
	public void setDefaultBranch(String defaultBranch) {
		this.defaultBranch = defaultBranch;
	}
	public String getMailToBranch() {
		return mailToBranch;
	}
	public void setMailToBranch(String mailToBranch) {
		this.mailToBranch = mailToBranch;
	}
	public String getAnalysisCode() {
		return analysisCode;
	}
	public void setAnalysisCode(String analysisCode) {
		this.analysisCode = analysisCode;
	}
	public String getBankCode1() {
		return bankCode1;
	}
	public void setBankCode1(String bankCode1) {
		this.bankCode1 = bankCode1;
	}
	public String getBankCode2() {
		return bankCode2;
	}
	public void setBankCode2(String bankCode2) {
		this.bankCode2 = bankCode2;
	}
	public String getBankCode3() {
		return bankCode3;
	}
	public void setBankCode3(String bankCode3) {
		this.bankCode3 = bankCode3;
	}
	public String getBankCode4() {
		return bankCode4;
	}
	public void setBankCode4(String bankCode4) {
		this.bankCode4 = bankCode4;
	}
	public String getCustomerGroup() {
		return customerGroup;
	}
	public void setCustomerGroup(String customerGroup) {
		this.customerGroup = customerGroup;
	}
	public String getGroupDescription() {
		return groupDescription;
	}
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}
	public String getDataMaintained() {
		return dataMaintained;
	}
	public void setDataMaintained(String dataMaintained) {
		this.dataMaintained = dataMaintained;
	}
	public String getMidasFacilityAllow() {
		return midasFacilityAllow;
	}
	public void setMidasFacilityAllow(String midasFacilityAllow) {
		this.midasFacilityAllow = midasFacilityAllow;
	}
	public int getClearingId() {
		return clearingId;
	}
	public void setClearingId(int clearingId) {
		this.clearingId = clearingId;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	

	public Set<AddressDetails> getAddressDetails() {
		return addressDetails;
	}


	public void setAddressDetails(Set<AddressDetails> addressDetails) {
		this.addressDetails = addressDetails;
	}


	public Set<OtherDetails> getOtherDetails() {
		return otherDetails;
	}


	public void setOtherDetails(Set<OtherDetails> otherDetails) {
		this.otherDetails = otherDetails;
	}


	public void setId(long id) {
		this.id = id;
	}
	
	


	public Set<SpecialInstructionDetails> getSpecialInstructionDetails() {
		return specialInstructionDetails;
	}


	public void setSpecialInstructionDetails(Set<SpecialInstructionDetails> specialInstructionDetails) {
		this.specialInstructionDetails = specialInstructionDetails;
	}


	@Override
	public String toString() {
		return "Customer [id=" + id + ", fullName=" + fullName + ", customerNumber=" + customerNumber + ", shortName="
				+ shortName + ", customerType=" + customerType + ", blocked=" + blocked + ", closed=" + closed
				+ ", deceased=" + deceased + ", inactive=" + inactive + ", accountOfficer=" + accountOfficer
				+ ", reference=" + reference + ", language=" + language + ", parentCountry=" + parentCountry
				+ ", riskCountry=" + riskCountry + ", residenceCountry=" + residenceCountry + ", defaultBranch="
				+ defaultBranch + ", mailToBranch=" + mailToBranch + ", analysisCode=" + analysisCode + ", bankCode1="
				+ bankCode1 + ", bankCode2=" + bankCode2 + ", bankCode3=" + bankCode3 + ", bankCode4=" + bankCode4
				+ ", customerGroup=" + customerGroup + ", groupDescription=" + groupDescription + ", dataMaintained="
				+ dataMaintained + ", midasFacilityAllow=" + midasFacilityAllow + ", clearingId=" + clearingId
				+ ", location=" + location + ", addressDetails=" + addressDetails + ", otherDetails=" + otherDetails
				+ ", specialInstructionDetails=" + specialInstructionDetails + "]";
	}

	
}
