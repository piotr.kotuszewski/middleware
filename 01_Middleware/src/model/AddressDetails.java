package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="address_details")
public class AddressDetails implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_address_details")
	private long id;
	@Column(name="address_type")
	private String addressType;
	@Column(name="address_id")
	private String addressId;
	private String salutation;
	@Column(name="name_and_address")
	private String nameAndAddress;
	@Column(name="zip_code")
	private String zipCode;
	private String language;
	private String locale;
	private String phone;
	private String fax;
	private String telex;
	private String email;
	@Column(name="swift_address")
	private String swiftAddress;
	@Column(name="transfer_method")
	private String transferMethod;
	@Column(name="addressee_customer")
	private String addresseeCustomer;
	@Column(name="number_of_copies")
	private int numberOfCopies;
	@Column(name="originals_number")
	private int originalsNumber;
	
	@ManyToOne
	@JoinColumn(name="id_customer")
	//@JsonIgnore
	private Customer customer;
	
	public AddressDetails() {}

	public AddressDetails(String addressType, String addressId, String salutation, String nameAndAddress,
			String zipCode, String language, String locale, String phone, String fax, String telex, String email,
			String swiftAddress, String transferMethod, String addresseeCustomer, int numberOfCopies,
			int originalsNumber) {
		super();
		this.addressType = addressType;
		this.addressId = addressId;
		this.salutation = salutation;
		this.nameAndAddress = nameAndAddress;
		this.zipCode = zipCode;
		this.language = language;
		this.locale = locale;
		this.phone = phone;
		this.fax = fax;
		this.telex = telex;
		this.email = email;
		this.swiftAddress = swiftAddress;
		this.transferMethod = transferMethod;
		this.addresseeCustomer = addresseeCustomer;
		this.numberOfCopies = numberOfCopies;
		this.originalsNumber = originalsNumber;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getNameAndAddress() {
		return nameAndAddress;
	}

	public void setNameAndAddress(String nameAndAddress) {
		this.nameAndAddress = nameAndAddress;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getTelex() {
		return telex;
	}

	public void setTelex(String telex) {
		this.telex = telex;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSwiftAddress() {
		return swiftAddress;
	}

	public void setSwiftAddress(String swiftAddress) {
		this.swiftAddress = swiftAddress;
	}

	public String getTransferMethod() {
		return transferMethod;
	}

	public void setTransferMethod(String transferMethod) {
		this.transferMethod = transferMethod;
	}

	public String getAddresseeCustomer() {
		return addresseeCustomer;
	}

	public void setAddresseeCustomer(String addresseeCustomer) {
		this.addresseeCustomer = addresseeCustomer;
	}

	public int getNumberOfCopies() {
		return numberOfCopies;
	}

	public void setNumberOfCopies(int numberOfCopies) {
		this.numberOfCopies = numberOfCopies;
	}

	public int getOriginalsNumber() {
		return originalsNumber;
	}

	public void setOriginalsNumber(int originalsNumber) {
		this.originalsNumber = originalsNumber;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "AddressDetails [id=" + id + ", addressType=" + addressType + ", addressId=" + addressId
				+ ", salutation=" + salutation + ", nameAndAddress=" + nameAndAddress + ", zipCode=" + zipCode
				+ ", language=" + language + ", locale=" + locale + ", phone=" + phone + ", fax=" + fax + ", telex="
				+ telex + ", email=" + email + ", swiftAddress=" + swiftAddress + ", transferMethod=" + transferMethod
				+ ", addresseeCustomer=" + addresseeCustomer + ", numberOfCopies=" + numberOfCopies
				+ ", originalsNumber=" + originalsNumber + "]";
	}

	
}
