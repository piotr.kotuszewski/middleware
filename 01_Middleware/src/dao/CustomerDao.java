package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Customer;

public class CustomerDao {
	
	private EntityManagerFactory emFactory;
	
	public CustomerDao() {
		emFactory = Persistence.createEntityManagerFactory("01_Middleware");
	}
	
	public Customer get(long id) {
		EntityManager entityManager = emFactory.createEntityManager();
		Customer customer = entityManager.find(Customer.class, id);
		entityManager.close();
		emFactory.close();
		return customer;
	}

	public void closeFactory() {
		emFactory.close();
	}
}
