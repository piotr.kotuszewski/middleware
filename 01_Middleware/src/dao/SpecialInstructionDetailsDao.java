package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.AddressDetails;
import model.SpecialInstructionDetails;

public class SpecialInstructionDetailsDao {
	private EntityManagerFactory emFactory;
	
	public SpecialInstructionDetailsDao() {
		emFactory = Persistence.createEntityManagerFactory("01_Middleware");
	}
	
	public SpecialInstructionDetails get(long id) {
		EntityManager entityManager = emFactory.createEntityManager();
		SpecialInstructionDetails specialIns = entityManager.find(SpecialInstructionDetails.class, id);
		entityManager.close();
		emFactory.close();
		return specialIns;
	}
	
	public void closeFactory() {
		emFactory.close();
	}
}
