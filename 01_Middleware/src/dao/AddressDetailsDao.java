package dao;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.AddressDetails;

public class AddressDetailsDao {
		
		private EntityManagerFactory emFactory;
		
		public AddressDetailsDao() {
			emFactory = Persistence.createEntityManagerFactory("01_Middleware");
		}
		
		public AddressDetails get(long id) {
			EntityManager entityManager = emFactory.createEntityManager();
			AddressDetails addressDetails = entityManager.find(AddressDetails.class, id);
			entityManager.close();
			emFactory.close();
			return addressDetails;
		}
		
		public List<AddressDetails> getByCustomerId(long id) {
			EntityManager entityManager = emFactory.createEntityManager();
			Query query = entityManager.createQuery("SELECT e FROM address_details e WHERE e.id_customer = :id");
			query.setParameter("id", id);
			List<AddressDetails> addressDetails = query.getResultList();
			entityManager.close();
			return addressDetails;
		}
		
		public void closeFactory() {
			emFactory.close();
		}
}
